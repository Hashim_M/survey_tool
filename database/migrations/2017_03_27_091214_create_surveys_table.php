<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('survey_type_id')->unsigned();
            $table->integer('reviewer_id')->unsigned();
            $table->string('task_name');
            $table->integer('council_id')->unsigned();
            $table->boolean('status_id')->default(0);
            $table->integer('service_area_id')->unsigned();
            $table->integer('year')->nullable();
            $table->integer('mobile');
            $table->text('notes');
            $table->tinyInteger('multipart');
            $table->timestamp('deadline')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surveys');
    }
}
