<?php



Route::get('/', function () {
    return view('layouts.master');
});
Route::group(['middleware' => 'visitor'], function (){
    Route::get('/register', 'RegisterController@register');
    Route::post('/register', 'RegisterController@post');
    Route::get('/login', 'LoginController@login');
    Route::post('login', 'LoginController@post');
    Route::get('/forgot-password', 'ForgotPasswordController@forgotPassword');
    Route::post('/forgot-password', 'ForgotPasswordController@post');
    Route::get('/reset/{email}/{resetCode}','ForgotPasswordController@reset');
    Route::post('/reset/{email}/{resetCode}','ForgotPasswordController@postReset');
});

Route::group(['middleware' => 'admin'], function (){
    Route::get('/index', 'AdminController@index');
    Route::get('/createreviewer', 'ReviewersController@create');
    Route::post('/store', 'ReviewersController@storeReviewer');
    Route::get('/reviewers', 'ReviewersController@index');
    Route::get('/delete/{id}', 'ReviewersController@destroy');
    Route::get('/update/{id}', 'ReviewersController@update');
    Route::get('/edit/{id}', 'ReviewersController@edit');
    Route::get('/surveys', 'SurveyController@index');
    Route::get('/create', 'SurveyController@create');
    Route::post('/storesurvey', 'SurveyController@store');
    Route::get('/reviewersurveys/{id}', 'ReviewersController@surveysIndex');
    Route::get('/assign/{reviewer_id}/{survey_id}', 'SurveyController@assign');
    Route::get('/editsurvey/{id}', 'SurveyController@edit');
    Route::post('/updatesurvey/{id}', 'SurveyController@update');

});
Route::group(['middleware' => 'reviewer'], function(){
   Route::get('index', 'ReviewersController@reviewerIndex');
});

Route::post('/logout', 'LoginController@logout');
Route::get('/activation/{email}/{activationCode}', 'ActivationController@activate');