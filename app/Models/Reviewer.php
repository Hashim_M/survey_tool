<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    protected $table = 'surveys';
    protected $fillable = [
        'user_id',
        'role_id',
        'council_id',
    ];

    public function surveys(){
        return $this->hasMany('App\Models\Survey', 'survey_id');
    }
}
