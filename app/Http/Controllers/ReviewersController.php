<?php

namespace App\Http\Controllers;

use App\Models\Reviewer;
use Illuminate\Http\Request;
use Sentinel;
use Activation;
use App\Models\User;
use Mail;
use App\Models\Survey;

class ReviewersController extends Controller
{
    public $password;
    public function create(){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, 12 );
        return view('admin.reviewers.create', compact('password'));
    }

    public function index(){
        $role = Sentinel::findRoleBySlug('reviewer');
        $users = $role->users()->with('roles')->get();
        return view('admin.reviewers.index', compact('users'));
    }

    public function storeReviewer(Request $request) {
        $this->validate($request, [
            'first_name' => 'alpha|required|min:5|max:255',
            'last_name' => 'alpha|required|min:5|max:255',
            'email' => 'required|email|max:255',
        ]);
        $this->password = $request->input('password');
        $user = Sentinel::register($request->all());
        $activate = Activation::create($user);
        $role = Sentinel::findRoleBySlug('reviewer');
        $role->users()->attach($user);
        $this->sendMail($user, $activate->code);
        return redirect()->back()->with(['success' => 'Reviewer Created. Password and Activation code Sent to email']);
    }

    private function sendMail($user, $code){
        Mail::send('mail.activation', [
            'user' => $user, 'code' => $code, 'password' => $this->password,
        ], function ($message) use ($user){
            $message->to($user->email);
            $message->subject("SOCITM Account Activation");
        });
    }

    public function destroy($id){
        $user = Sentinel::findById($id);
        $user->delete();
        return redirect()->back()->with(['success' => 'Reviewer Deleted']);
    }

    public function edit($id){
        $user = Sentinel::findById($id);
        return view('admin.reviewers.edit', compact('user'));
    }

    public function update($id, Request $request) {
        $user = Sentinel::findById($id);
        $credentials = [
          'first_name' => $request->input('first_name'),
          'last_name' => $request->input('last_name'),
          'email' => $request->input('email'),
        ];

        $user = Sentinel::update($user, $credentials);
        return redirect()->back()->with(['success' => 'Reviewer updated']);
    }

    public function surveysIndex($id){
        $reviewer = Reviewer::find($id);
        $reviewer_surveys = Survey::where('reviewer_id', $id)->get();
        $surveys = Survey::all();
        return view('admin.reviewers.surveys.index', compact('surveys','reviewer_surveys', 'reviewer'));
    }

    public function reviewerIndex(){
        return view('reviewer.index');
    }

}
