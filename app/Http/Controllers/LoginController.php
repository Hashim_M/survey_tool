<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Sentinel;

class LoginController extends Controller
{
    public function login(){
        return view('auth.login');
    }

    public function post(Request $request){
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
        ]);
        try {
            if(Sentinel::authenticate($request->all())){
                $slug = Sentinel::getUser()->roles()->first()->slug;
                if($slug == 'admin')
                    return redirect('/surveys');

                    return redirect('/');
            }else{
                return redirect()->back()->with(['error' => 'Your email/password were Incorrect']);
            }
        }catch (ThrottlingException $exception){
            $delay_time = $exception->getDelay();
            return redirect()->back()->with(['error' => "Please try after $delay_time to sign in"]);
        }catch (NotActivatedException $exception){
            return redirect()->back()->with(['error' => "Your account is Not Activated Please check email to activate"]);
        }
    }

    public function logout(){
        Sentinel::logout();
        return redirect('/login');
    }
}
