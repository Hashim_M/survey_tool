<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey;
use App\Models\CouncilType;
use App\Models\Reviewer;
use App\Models\ServiceArea;

class SurveyController extends Controller
{
    public function index(Request $request){
        $all_surveys = Survey::all();
        $name = $request->input('name');
        $surveys = Survey::where('name', 'like', '%' .$name . '%')->get();
        return view('admin.index', compact('all_surveys', 'surveys'));
    }

    public function create(){
        $service_areas = ServiceArea::all();
        $council_types = CouncilType::all();
        return view('admin.surveys.create', compact('council_types', 'service_areas'));
    }

    public function store(Request $request){
        $survey = new Survey();
        $inputs = $request->all();
        $survey->create($inputs);
        return redirect()->back()->with(['success' => 'Survey Created']);
    }

    public function assign($reviewer_id, $survey_id){
        $survey = Survey::find($survey_id);
        $survey->reviewer_id = $reviewer_id;
        $survey->update();
        return redirect()->back()->with(['success' => 'Survey Assigned']);
    }

    public function update(Request $request, $id){
        $survey = Survey::find($id);
        $inputs = $request->all();
        $survey->update($inputs);
        return redirect()->back()->with(['success' => 'Survey Updated']);
    }

    public function edit($id){
        $survey = find($id);
        return view('admin.surveys.edit', compact('survey'));
    }

}
