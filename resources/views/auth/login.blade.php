@extends('layouts.master')
@section('content')
<div class="row">
        <div class="col-md-5 col-md-offset-4 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Please Sign in</h2>
                </div>
                <div class="panel-body">
                    <form action="/login" method="post">
                        {{ csrf_field() }}
                        @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @elseif(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                    <input class="form-control" type="email" name="email" id="email" placeholder="email" required>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input class="form-control" type="password" name="password" id="password" placeholder="Password" required>
                                </span>
                            </div>
                        </div>
                        <a href="/forgot-password">Forgot your password?</a>
                        <div class="form-group">
                            <button class="btn btn-default pull-right" type="submit">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
@endsection
