<h2>Hello {{ $user->first_name }}</h2>
<p>
    Please click the following link to reset your password,
    <a href="http://survey.dev/reset/{{ $user->email }}/{{ $code }}">Reset Account</a>
</p>