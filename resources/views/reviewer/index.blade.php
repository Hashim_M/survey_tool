@extends('layouts.master')
@section('content')
    <div class="row">
        <table class="table table-responsive table-stripped">
            <thead>
            <tr>
                <th>Survey</th>
                <th class="col-sm-1">Deadline</th>
                <th class="col-sm-1">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reviewer_surveys as $reviewer_survey)
                <tr>
                    <td>{{ $reviewer_survey->name }}</td>
                    <td>{{ $reviewer_survey->deadline }}</td>
                    <td><a href="#">Manage</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection