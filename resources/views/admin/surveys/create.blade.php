@extends('layouts.master')
@section('content')
    <div class="row">
        <form action="/storesurvey" method="post">
            @include('admin.surveys.form')
        </form>
    </div>
@endsection