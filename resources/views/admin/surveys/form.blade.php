     {{ csrf_field() }}
     <div class="col-md-8 col-md-offset-2 col-sm-12">
         @if(session('success'))
             <div class="alert alert-success alert-dismissible fade " role="alert">
                 <button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session('success') }}
             </div>
         @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Survey Details</h2>
                </div>
                <div class="panel-body">
                        <div class="form-group">
                            <div class="input-group">
                                <label for="name">Survey Name</label>
                                <input class="form-control" type="text" name="name" id="name" placeholder="Survey Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label for="year">Year</label>
                                <select name="year" class="form-control">
                                    <option>Year</option>
                                    <option>2017</option>
                                    <option>2016</option>
                                    <option>2015</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label for="name">Deadline Date</label>
                                <input class="form-control" type="date" name="deadline" id="deadline" placeholder="Deadline Date" required>
                            </div>
                        </div>
                        <legend>Council types to take the survey</legend>
                    <div class="row">
                            <div class="form-group">
                                @foreach($council_types as $council_type)
                                <label style="margin: 8px;">
                                    <input type="checkbox" name="council"> {{ $council_type->name }}
                                </label>
                                @endforeach
                            </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Task Details</h2>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="input-group">
                                <label for="">Task Name</label>
                                    <input class="form-control" type="text" name="task_name" id="task_name" placeholder="Task Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label for="service_area">Service Area</label>
                                <select name="service_area" class="form-control">
                                    <option>Service Area</option>
                                    @foreach($service_areas as $service_area)
                                    <option value="{{ $service_area->id }}">{{ $service_area->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label for="notes">Task notes</label>
                                <textarea class="form-control" rows="6" cols="20" name="notes"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="mobile"> Mobile task
                            </label>
                        </div>
                    </div>
                </div>
                 <div class="form-group">
                     <button class="btn btn-success pull-right" type="submit">Submit</button>
                 </div>
                </div>
