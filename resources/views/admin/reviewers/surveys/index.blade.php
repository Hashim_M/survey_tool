@extends('layouts.master')
@section('content')
<h3>
    <strong>Reviewer:</strong>
</h3>
<ol class="breadcrumb">
    <li><a href="/reviewers">Reviewers</a></li>
    <li class="active">Manage reviewer's survey</li>
</ol>
    <h3>Reviewer's Surveys</h3>
<div class="row">
    <table class="table table-responsive table-stripped">
        <thead>
        <tr>
            <th>Survey</th>
            <th class="col-sm-1">Deadline</th>
            <th class="col-sm-1">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reviewer_surveys as $reviewer_survey)
            <tr>
                <td>{{ $reviewer_survey->name }}</td>
                <td>{{ $reviewer_survey->deadline }}</td>
                <td><a href="#">Manage</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <h3>Surveys not Assigned to reviewer</h3>
    <table class="table">
        <thead>
            <tr>
                <th>Survey</th>
                <th class="col-sm-1">Deadline</th>
                <th class="col-sm-1">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($surveys as $survey)
            @if(!isset($survey->reviewer_id))
            <tr>
                 <td>{{ $survey->name }}</td>
                 <td>{{ date($survey->deadline) }}</td>
                 <td>
                     <a href="{{ url('/assign', $survey->reviewer_id, $survey->id) }}">Assign</a>
                 </td>
            </tr>
            @endif
        @endforeach
       </tbody>
    </table>
</div>
@endsection