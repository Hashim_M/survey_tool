@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Add Reviewer</h2>
                </div>
                <div class="panel-body">
                    <form action="/store" method="post">
                        {{ csrf_field() }}
                        @if(session('success'))
                         <div class="alert alert-success">
                             {{ session('success') }}
                         </div>
                        @endif
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" type="text" name="first_name" id="first_name" placeholder="First Name" required>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" type="text" name="last_name" id="last_name" placeholder="Last Name" required>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input class="form-control" type="email" name="email" id="email" placeholder="email" required>
                                </span>
                            </div>
                        </div>
                            <input class="form-control" type="hidden" name="password" id="password" placeholder="Password" value="{!! $password !!}">
                        <div class="form-group">
                            <button class="btn btn-default pull-right" type="submit">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection